#include<stdio.h>

int main()
{
    int an;
    printf("an=");
    scanf("%d", &an);
    
    if ((an % 400 == 0) || (an % 4 == 0 && an % 100 != 0))
    {
        printf("%d este an bisect", an);
    }
    else
    {
        printf("%d nu este an bisect", an);
    }
    
    return 0;
}