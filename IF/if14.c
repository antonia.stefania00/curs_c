#include<stdio.h>

int main()
{
    int x;
    int y;
    printf("x=");
    scanf("%d", &x);
    printf("y=");
    scanf("%d", &y);
    if (x>0 && y>0)
    {
        printf("Cadranul 1");
    }
    else if (x>0 && y<0)
    {
        printf("Cadranul 4");
    }
    else if (x<0 && y>0)
    {
        printf("Cadranul 2");
    }
    else if (x<0 && y<0)
    {
        printf("Cadranul 3");
    }
    else if (x>0 && y==0)
    {    
        printf("Semiaxa 1-4");
    }
    else if (x<0 && y==0)
    {
        printf("Semiaxa 2-3");
    }
    else if (x==0 && y>0)
    {
        printf("Semiaxa 1-2");
    }
    else if (x==0 && y<0)
    {
        printf("Semiaxa 3-4");
    }
    else if (x==0 && y==0)
    {
        printf("Origine");
    }
    
    return 0;
}