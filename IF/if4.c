#include<stdio.h>

int main()
{
    int a;
    printf("a=");
    scanf("%d", &a);
    
    if (a % 5 == 0 || a % 11 == 1)
    {
        printf("%d este divizibil cu 5 sau 11", a);
    }
    else
    {
        printf("%d nu este divizibil cu 5 sau 11", a);
    }
    
    return 0;
}