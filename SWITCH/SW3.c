#include <stdio.h>
int main()
{
    char op;
    int a;
    int b;
    
    printf("op= ");
    scanf("%c", &op);
    printf("a= ");
    scanf("%d", &a);
    printf("b= ");
    scanf("%d", &b);

    switch(op){
        case '+':
            printf("%d + %d = %d", a,b,(a+b));
            break;
        case '-':
            printf("%d - %d = %d", a,b,(a-b));
            break;
        case '*':
            printf("%d*%d = %d", a,b,(a*b));
            break;
        case '/':
            printf("%d/%d = %d", a,b,(a/b));
            break;
    }
return 0;
}