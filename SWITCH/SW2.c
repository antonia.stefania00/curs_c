#include <stdio.h>
int main()
{
    int ziua;
    
    printf("Introduceti ziua: ");
    scanf("%d", &ziua);
    
    switch(ziua){
        case 1:
            printf("Luni");
            break;
        case 2:
            printf("Marti");
            break;
        case 3:
            printf("Miercuri");
            break;
        case 4:
            printf("Joi");
            break;
        case 5:
            printf("Vineri");
            break;
        case 6:
            printf("Sambata");
            break;
        case 7:
            printf("Duminica");
            break;
        default:
            printf("Zi invalida");
    }

    return 0;
}