#include <stdio.h>
int main()
{
    char litera;

    printf("Introduceti litera: ");
    scanf("%c", &litera);

    switch(litera){
        case 'a':
            printf("a este vocala");
            break;
        case 'e':
            printf("e este vocala");
            break;
        case 'i':
            printf("i este vocala");
            break;
        case 'o':
            printf("o este vocala");
            break;
        case 'u':
            printf("u este vocala");
            break;
        default:
            printf("Nu este vocala");
    }

    return 0;
}